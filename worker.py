from rq import Connection, Queue, Worker
from main import CONNECTION

if __name__ == "__main__":
    with Connection(CONNECTION):
        worker = Worker(list(map(Queue, ["default"])))
        worker.work()
