from rq_scheduler import Scheduler
from rq import Connection
from main import CONNECTION, q


if __name__ == "__main__":
    with Connection(CONNECTION):
        scheduler = Scheduler(queue=q, connection=CONNECTION, interval=30)
        for job in scheduler.get_jobs():
            scheduler.cancel(job)
        scheduler.run()
