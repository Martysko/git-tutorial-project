FROM python:3.9

ADD requirements.txt requirements.txt

RUN python3 -m pip install -U pip
RUN python3 -m pip install -r requirements.txt

ADD . .

EXPOSE 5000

CMD python3 main.py

