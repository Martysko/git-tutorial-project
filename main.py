import os
import psycopg2
from flask import Flask, request
from redis import Redis
from rq import Queue
from rq.job import Job
from rq_scheduler import Scheduler
app = Flask(__name__)
from dotenv import load_dotenv
from example_function import example_function

load_dotenv()

HOST_URL = os.getenv('HOST_URL')

CONNECTION = Redis(host=HOST_URL, port=6380)

q = Queue(connection=CONNECTION)

scheduler = Scheduler(queue=q, connection=CONNECTION, interval=30)


@app.route('/create_table', methods=['GET', 'POST'])
def create_table():
    with psycopg2.connect(
            host=HOST_URL,
            database="postgres",
            user="postgres",
            password="postgres",
            port=5440) as conn:

        try:
            cursor = conn.cursor()
            cursor.execute('drop table records;')
        except:
            pass
        finally:
            cursor.close()

    with psycopg2.connect(
            host=HOST_URL,
            database="postgres",
            user="postgres",
            password="postgres",
            port=5440) as conn:

        try:
            cursor = conn.cursor()
            cursor.execute(f"create table records (id integer, text varchar)")
            return "Table created"
        except:
            return "Problem"
        finally:
            cursor.close()


@app.route('/add_record', methods=['GET', 'POST'])
def add_record():
    with psycopg2.connect(
            host=HOST_URL,
            database="postgres",
            user="postgres",
            password="postgres",
            port=5440) as conn:

        cursor = conn.cursor()

        record = request.values.get('data')

        cursor.execute(f"insert into records (text) VALUES('{record}')")

        cursor.execute(f"select * from records;")
        results = cursor.fetchall()

        cursor.close()

        return {"response": results}


@app.route('/add_job', methods=['GET', 'POST'])
def add_job():

    data = request.values.get('data')
    job = q.enqueue_call(example_function, args=(data,))

    return {"job_id": job.get_id()}


@app.route('/add_cronjob', methods=['GET', 'POST'])
def add_cronjob():

    data = request.values.get('data')

    job = scheduler.cron('* * * * *', example_function, args=(data,))

    return {'active_jobs': len(list(scheduler.get_jobs())),
            'job_id': job.id}


@app.route('/remove_all_cronjobs', methods=['GET', 'POST'])
def remove_all_cronjob():

    cronjobs_len = len(list(scheduler.get_jobs()))

    for job in scheduler.get_jobs():
        scheduler.cancel(job)

    return {'removed_cronjobs': cronjobs_len}


@app.route('/get_result/<job_id>', methods=['GET'])
def get_result(job_id):
    job = Job.fetch(job_id, CONNECTION)
    return {'result': job.result}


if __name__ == '__main__':
    app.run(host='0.0.0.0')

